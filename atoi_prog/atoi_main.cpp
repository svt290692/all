#include <iostream>
using namespace std;

int get_intFromStr(const char * pStr) {
	int iBack = 0;
		
	if(pStr) {
		size_t iLen = strlen(pStr);

		while(*pStr) {
			iBack += (((*pStr - '0') % 10) * pow(10, --iLen));
			pStr++;
		}
	}
	
	return iBack;
}

int get_intFromStr_r(const char * pStr, size_t iLen) {return ((*pStr) ? ((pow(10, (iLen - 1)) * ((*pStr - '0') % 10)) + (get_intFromStr_r((pStr + sizeof(char)), (iLen - 1)))) : 0);}

int get_intFromStr_r(const char * pStr) {
	int iBack = 0;
	
	if(pStr) {
		iBack = get_intFromStr_r(pStr, strlen(pStr));
	}
	
	return iBack;
}

int main (int argc, char ** argv) {
	#define STR_PARAM "72511226"
	std::cout << "\natoi(" << STR_PARAM << ')' << " == " << get_intFromStr_r(STR_PARAM) << std::endl; 

	int iPause;
	std::cin >> iPause;

	return 0;
}