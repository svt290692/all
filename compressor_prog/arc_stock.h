#ifndef _ARC_STOCK_H_INCLUDED
	#define _ARC_STOCK_H_INCLUDED

#define def_arrayLen(Array) (sizeof(Array) / sizeof(Array[0]))
#define def_strLen(pStr) (def_arrayLen(pStr) - 1)

#include <iostream>

char * skip_strDataPart (const char *);
size_t gen_bitMaskByShift(size_t, size_t);
const char * gen_str_equal(FILE *, size_t &); // need delete [] after

#endif // _ARC_STOCK_H_INCLUDED