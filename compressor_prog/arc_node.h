#ifndef _ARC_NODE_H_INCLUDED
	#define _ARC_NODE_H_INCLUDED

#include <deque>
#include "arc_types.h"

class CNode;
typedef class CNode * PNODE;
typedef class CNode & RNODE;

class CNode {
public:
	explicit CNode(void);
	~CNode(void);

	float get_freq (void) const;
	PNODE get_left (void) const;
	PNODE get_right (void) const;
	UCHAR get_byte (void) const;

	void set_freq (float);
	void set_left (PNODE);
	void set_right (PNODE);
	void set_byte (UCHAR);
	void set_mask (size_t);
	void set_mask_sz (size_t);
	size_t get_mask (void) const;
	size_t get_mask_sz (void) const;

	template <typename T> static void sort(T & x, int iElems) {
		size_t iSwap = 0;
		--iElems;
		int i;
		PNODE pBuf;

		while(true) {
			for(i = 0; i < iElems; ++i) {
				if((x[i] -> get_freq()) < (x[(i + 1)] -> get_freq())) {
					pBuf = x[i];
					x[i] = x[(i + 1)];
					x[(i + 1)] = pBuf;
					iSwap += 1u;
				}
			}

			if(iSwap == 0) {
				break;
			} else {
				iSwap = 0;
			}
		}
	};

	static PNODE find_r (const PNODE, UCHAR);

private:
	UCHAR m_byte;
	float m_fFreq;
	size_t m_iMask;
	size_t m_iMaskLen;
	PNODE m_pLeft;
	PNODE m_pRight;

	explicit CNode(const PNODE);
	explicit CNode(const RNODE);
};

#endif // _ARC_NODE_H_INCLUDED