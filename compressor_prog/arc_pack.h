#ifndef _ARC_PACK_H_INCLUDED
	#define _ARC_PACK_H_INCLUDED

#include <iostream>
#include "arc_node.h"
#include <deque>
#include "arc_types.h"

#define HAFFMAN_EXTENSION ".haffman" // compressed, decompressed == EMPTY
#define HAFFMAN_DEFAULT_OUT_NAME "out"
#define HAFFMAN_FIRST_INFO "?(04)haffman[0.1/TL]"
#define HAFFMAN_BUF_FILENAME "buffile.hffmn"

class CPack;
typedef class CPack * PPACK;
typedef class CPack & RPACK;
typedef std::deque<PNODE> PACK_CONTAINER;

union uShiftTypization {
	void * p;
	float f;
	float * pf;
	int i;
	int * pi;
};

class CPack {
public:
	enum eErrors {
		ERR_PARAM = 1,
		ERR_NO_PARAM,
		ERR_PARAM_FORMAT,
		ERR_NO_BUNIQUE_PARAM,
		ERR_EOF,
		ERR_TREE_RESULT,
		ERR_BUF,
		ERR_STR_GEN
	};

	enum eByte {MAX_BYTES = 256};

	explicit CPack (void);
	~CPack (void);

	void decompress (const char *, size_t, const char *); // decompress line
	void decompress (FILE *, size_t, const char *); // decompress file

	void compress (const char *, size_t, const char *); // compress line
	void compress (FILE *, size_t, const char *); // compress file

	float get_cratio(void) const;

private:
	// compress
	PNODE m_pBaseNode_head; // contain base tree
	PACK_CONTAINER m_deqBaseNodes; // all base nodes

	size_t m_aiByteMaskLen[CPack::MAX_BYTES];
	size_t m_aiByteMask[CPack::MAX_BYTES];
	size_t m_aiByteCount[CPack::MAX_BYTES];
	float m_afByteFreq[CPack::MAX_BYTES];
	
	// decompress
	FILE * m_pBufFile;

	// both
	PACK_CONTAINER m_deqLay;
	PACK_CONTAINER m_deqPercentNodes; // all sorted nodes
	size_t m_iBeforePack; // 'in' len 
	size_t m_iAfterPack; // 'out' len
	PNODE m_pPercentNode_head; // tree sorted by %
	FILE * m_pOutPut; // (co/deco)mpressed file
	UCHAR m_iNullBits; // null bits count at the end of pack
	float m_fRatio; // (co/deco)mpressed ratio
	const char * m_psSrc; // input str
	
	// compress
	void c_expand_info (void);
	void c_make_codes_r (PNODE);
	const char * c_gen_prefix_word (UCHAR) const;
	void c_fill_out (void);

	static const char * c_gen_prefix_word(size_t, size_t);

	// decompress

	// both
	template <typename T> PNODE build_ptree_r(T & x, int iElems) {
		PNODE pBack = NULL;
	
		if(iElems >= 3) {
			CNode::sort(x, iElems);
			pBack = new class CNode;
			get_ldeq().push_back(pBack);
			pBack -> set_freq((x[(iElems - 2)] -> get_freq()) + (x[(iElems - 1)] -> get_freq()));
			pBack -> set_right(x[(iElems - 2)]);
			pBack -> set_left(x[(iElems - 1)]);
			x[(iElems - 2)] = pBack;
			pBack = build_ptree_r(x, (iElems - 1));
		} else {
			switch(iElems) {
				case 1: {
					pBack = x[0];

					break;
				} case 2: {
					pBack = new class CNode;
					get_ldeq().push_back(pBack);
					pBack -> set_freq(((x[0]) -> get_freq()) + ((x[1]) -> get_freq()));
					CNode::sort(x, iElems);
					pBack -> set_right(x[0]);
					pBack -> set_left(x[1]);
	
					break;
				} default: {
					break;
				}
			}
		}

		return pBack;
	};

	PNODE init_wing(UCHAR);
	PNODE build_btree_r (PNODE, UCHAR);

	void clear_bdeq(void);
	void clear_pdeq(void);
	void clear_ldeq(void);
	void clear_info(void);
	void reset_data (void); // reset all

	// get
	FILE * get_target (void) const;
	FILE * get_buf_file (void) const;
	UCHAR get_null_bc (void) const;
	const char * get_src (void) const;
	PACK_CONTAINER & get_ldeq(void);
	size_t get_mask (UCHAR) const;
	size_t get_mask_sz (UCHAR) const;
	size_t get_count (UCHAR) const;
	float get_freq (UCHAR) const;
	PNODE get_ptree (void) const;
	PACK_CONTAINER & get_pdeq(void);
	PACK_CONTAINER & get_bdeq(void);
	PNODE get_btree (void) const;
	size_t get_sz_begin (void) const;
	size_t get_sz_end (void) const;

	// set
	void set_buf_file (FILE *);
	void set_sz_begin (size_t);
	void set_sz_end (size_t);
	void set_btree (PNODE);
	void set_mask (UCHAR, size_t);
	void set_mask_sz (UCHAR, size_t);
	void set_count (UCHAR, size_t);
	void set_freq (UCHAR, float);
	void set_ptree (PNODE);
	void set_src (const char *);
	void set_target(FILE *);
	void set_null_bc (UCHAR);
	void set_cratio(float);

	// not used
	// PNODE find_node(UCHAR); // (definition in '.cpp')

	// hidden
	explicit CPack (const PPACK);
	explicit CPack (const RPACK);
};

#endif // _ARC_PACK_H_INCLUDED