#include <iostream>
#include "arc_node.h"

CNode::~CNode(void) {
	set_left(NULL);
	set_right(NULL);
}

CNode::CNode(void) {
	set_left(NULL);
	set_right(NULL);
	set_freq(0.0f);
	set_byte(0);
	set_mask(0);
	set_mask_sz(0);
}

size_t CNode::get_mask (void) const {return m_iMask;}
size_t CNode::get_mask_sz (void) const {return m_iMaskLen;}
UCHAR CNode::get_byte (void) const {return m_byte;}
PNODE CNode::get_left (void) const {return m_pLeft;}
PNODE CNode::get_right (void) const {return m_pRight;}
float CNode::get_freq (void) const {return m_fFreq;}

void CNode::set_mask (size_t _i) {m_iMask = _i;}
void CNode::set_mask_sz (size_t _i) {m_iMaskLen = _i;}
void CNode::set_byte (UCHAR _i) {m_byte = _i;}
void CNode::set_left (PNODE pNode) {m_pLeft = pNode;}
void CNode::set_right (PNODE pNode) {m_pRight = pNode;}
void CNode::set_freq (float _f) {m_fFreq = _f;}

PNODE CNode::find_r (const PNODE pTree, UCHAR cData) {
	PNODE pBack = NULL;
	
	if(pTree) {
		if(cData == (pTree -> get_byte())) {
			pBack = const_cast<PNODE>(pTree);
		} else {
			if(pTree -> get_left()) {
				pBack = CNode::find_r((pTree -> get_left()), cData);
			}

			if((pBack == NULL) && (pTree -> get_right())) {
				pBack = CNode::find_r((pTree -> get_right()), cData);
			}
		}
	}

	return pBack;
}