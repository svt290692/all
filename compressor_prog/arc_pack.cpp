#include "arc_pack.h"
#include "arc_stock.h"

// get
FILE * CPack::get_buf_file (void) const {return m_pBufFile;}
const char * CPack::get_src (void) const {return m_psSrc;}
float CPack::get_cratio(void) const {return m_fRatio;}
UCHAR CPack::get_null_bc (void) const {return m_iNullBits;}
PACK_CONTAINER & CPack::get_ldeq(void) {return m_deqLay;}
size_t CPack::get_mask (UCHAR _i) const {return m_aiByteMask[_i];}
size_t CPack::get_mask_sz (UCHAR _i) const {return m_aiByteMaskLen[_i];}
size_t CPack::get_count (UCHAR _i) const {return m_aiByteCount[_i];}
float CPack::get_freq (UCHAR _i) const {return m_afByteFreq[_i];}
PACK_CONTAINER & CPack::get_pdeq(void) {return m_deqPercentNodes;}
PNODE CPack::get_ptree (void) const {return m_pPercentNode_head;}
PACK_CONTAINER & CPack::get_bdeq(void) {return m_deqBaseNodes;}
PNODE CPack::get_btree (void) const {return m_pBaseNode_head;}
FILE * CPack::get_target (void) const {return m_pOutPut;}
size_t CPack::get_sz_begin (void) const {return m_iBeforePack;}
size_t CPack::get_sz_end (void) const {return m_iAfterPack;}
// set
void CPack::set_buf_file (FILE * _p) {m_pBufFile = _p;}
void CPack::set_src (const char * _p) {m_psSrc = _p;}
void CPack::set_cratio(float _f) {m_fRatio = _f;}
void CPack::set_null_bc (UCHAR _i) {m_iNullBits = _i;}
void CPack::set_mask (UCHAR _i, size_t _var) {m_aiByteMask[_i] = _var;}
void CPack::set_mask_sz (UCHAR _i, size_t _var) {m_aiByteMaskLen[_i] = _var;}
void CPack::set_count (UCHAR _i, size_t _var) {m_aiByteCount[_i] = _var;}
void CPack::set_freq (UCHAR _i, float _var){m_afByteFreq[_i] = _var;}
void CPack::set_ptree (PNODE _p) {m_pPercentNode_head = _p;}
void CPack::set_btree (PNODE _p) {m_pBaseNode_head = _p;}
void CPack::set_target(FILE * _p) {m_pOutPut = _p;}
void CPack::set_sz_begin (size_t _i) {m_iBeforePack = _i;}
void CPack::set_sz_end (size_t _i) {m_iAfterPack = _i;}
// other 

void CPack::compress (FILE * pFile, size_t iMaxLen, const char * psName) {
	if(pFile) {
		const char * psResult = gen_str_equal(pFile, iMaxLen);
		compress(psResult, iMaxLen, psName);
		delete [] psResult;
	} else {
	}
}

void CPack::c_expand_info (void) {
	for(PACK_CONTAINER::iterator x = get_bdeq().begin(); x != get_bdeq().end(); ++x) {
		set_mask(((*x) -> get_byte()), ((*x) -> get_mask()));
		set_mask_sz(((*x) -> get_byte()), ((*x) -> get_mask_sz()));
	}
}

void CPack::clear_bdeq(void) {
	for(PACK_CONTAINER::iterator x = get_bdeq().begin(); x != get_bdeq().end(); ++x) {
		delete (*x);
	}

	get_bdeq().clear();
}

void CPack::clear_pdeq(void) {get_pdeq().clear();}

void CPack::clear_ldeq(void) {
	for(PACK_CONTAINER::iterator x = get_ldeq().begin(); x != get_ldeq().end(); ++x) {
		delete (*x);
	}

	get_ldeq().clear();
}

void CPack::clear_info(void) {
	UCHAR _byte = 0;
	
	while(true) {
		set_mask(_byte, 0);
		set_mask_sz(_byte, 0);
		set_count(_byte, 0);
		set_freq(_byte, 0.0f);

		_byte += 1;

		if(_byte == 0) {
			break;
		}
	}
}

const char * CPack::c_gen_prefix_word(size_t iMask, size_t iMaskLen) {
	static char sPrefix[(sizeof(size_t) * 8)];
	int i;
	int iPos = 0;

	for(i = (static_cast<int>(iMaskLen) - 1); i >= 0; --i) {
		if(iMask & (1 << i)) {
			sPrefix[iPos] = '1';
		} else {
			sPrefix[iPos] = '0';
		}

		++iPos;
	}

	sPrefix[iMaskLen] = '\0';

	return const_cast<const char *>(sPrefix);
}

const char * CPack::c_gen_prefix_word(UCHAR _uc) const {
	static char sPrefix[(sizeof(size_t) * 8)];
	int i;
	int iPos = 0;
	size_t iMask = get_mask(_uc);

	for(i = (static_cast<int>(get_mask_sz(_uc)) - 1); i >= 0; --i) {
		if(iMask & (1 << i)) {
			sPrefix[iPos] = '1';
		} else {
			sPrefix[iPos] = '0';
		}

		++iPos;
	}

	sPrefix[get_mask_sz(_uc)] = '\0';

	return const_cast<const char *>(sPrefix);
}

//PNODE CPack::find_node(UCHAR _i) {
//	PNODE pBack = NULL;
//	size_t i;
//
//	for(i = 0; i < get_bdeq().size(); i += 1u) {
//		if((get_bdeq()[i] -> get_byte()) == _i) {
//			pBack = get_bdeq()[i];
//
//			break;
//		}
//	}
//
//	return pBack;
//}

void CPack::c_make_codes_r (PNODE pNode) {
	if(pNode -> get_left()) {
		pNode -> get_left() -> set_mask((pNode -> get_mask()) << 1);
		pNode -> get_left() -> set_mask_sz((pNode -> get_mask_sz()) + 1u);
		c_make_codes_r(pNode -> get_left());
	}

	if(pNode -> get_right()) {
		pNode -> get_right() -> set_mask(((pNode -> get_mask()) << 1) | 1);
		pNode -> get_right() -> set_mask_sz((pNode -> get_mask_sz()) + 1u);
		c_make_codes_r(pNode -> get_right());
	}
}

PNODE CPack::init_wing(UCHAR _uc) {
	PNODE pBack = new class CNode;
	set_count(_uc, 1u);
	pBack -> set_byte(_uc);
	get_bdeq().push_back(pBack);

	return pBack;
}

PNODE CPack::build_btree_r (PNODE pTree, UCHAR cData) {
	PNODE pBack = NULL;
	
	if(pTree) {
		if(cData == (pTree -> get_byte())) {
			pBack = pTree;
			set_count(cData, (get_count(cData) + 1u));
		} else if(cData < (pTree -> get_byte())) {
			if(pTree -> get_left()) {
				pBack = build_btree_r((pTree -> get_left()), cData);
			} else {
				pTree -> set_left((pBack = init_wing(cData)));
			}
		} else {
			if(pTree -> get_right()) {
				pBack = build_btree_r((pTree -> get_right()), cData);
			} else {
				pTree -> set_right((pBack = init_wing(cData)));
			}
		}
	} else {
		pBack = init_wing(cData);
	}

	return pBack;
}

void CPack::decompress (const char * pSrc, size_t iMaxLen, const char * pNewName) {
	if(pSrc && iMaxLen && (get_src() == NULL)) {
		char * sFileName = NULL;

		if(pNewName) {
			size_t iLen = strlen(pNewName);

			if(iLen) {
				sFileName = new char [(iLen + 1u)];
				memcpy(sFileName, pNewName, iLen);
				sFileName[(iLen)] = '\0';
			} else {
			}
		} else {
			sFileName = new char [(def_strLen(HAFFMAN_DEFAULT_OUT_NAME) + 1u)];
			memcpy(sFileName, HAFFMAN_DEFAULT_OUT_NAME, def_strLen(HAFFMAN_DEFAULT_OUT_NAME));
			sFileName[def_strLen(HAFFMAN_DEFAULT_OUT_NAME)] = '\0';
		}

		FILE * pDeCompressed = fopen(sFileName, "wb");
		delete [] sFileName;
		sFileName = NULL;

		if(pDeCompressed) {
			set_src(pSrc);
			set_target(pDeCompressed);

			char psInfoCount[3];
			psInfoCount[0] = pSrc[def_strLen("?(")];
			psInfoCount[1] = pSrc[def_strLen("?(#")];
			psInfoCount[2] = '\0';

			int iDataCount = atoi((psInfoCount[0] == '0') ? (psInfoCount + 1) : psInfoCount);
			int iBytesUnique;
			int i;

			enum {MAX_PARAMLINE_LEN = 64};

			char sLine[MAX_PARAMLINE_LEN];
			fseek(get_buf_file(), (def_strLen(HAFFMAN_FIRST_INFO) + 1u), SEEK_SET); // ignore 1st line

			for(i = 0; i < iDataCount; ++i) {
				if(fgets(sLine, MAX_PARAMLINE_LEN, get_buf_file()) == NULL) {
					exit(CPack::ERR_NO_PARAM);
				}

				if(strncmp(sLine, "bc", def_strLen("bc")) == 0) {
					set_sz_end(static_cast<size_t>(atoi(sLine + def_strLen("bc") + 1u)));
				} else if(strncmp(sLine, "bu", def_strLen("bu")) == 0) {
					iBytesUnique = static_cast<int>(atoi(sLine + def_strLen("bu") + 1u));
				} else if(strncmp(sLine, "nb", def_strLen("nb")) == 0) {
					set_null_bc(static_cast<UCHAR>(atoi(sLine + def_strLen("nb") + 1u)));
				} else if (strncmp(sLine, "hs", def_strLen("hs")) == 0) { // hash sum
					// ignore
				} else {
					exit(CPack::ERR_PARAM);
				}
			}
			
			int iResultByte;
			PNODE pNode;
			uShiftTypization uType;

			for(i = 0; i < iBytesUnique; ++i) {
				if(fgets(sLine, MAX_PARAMLINE_LEN, get_buf_file()) == NULL) {
					exit(CPack::ERR_NO_BUNIQUE_PARAM);
				}

				if(sscanf(sLine, "%d %d", &iResultByte, &(uType.i)) != 2) {
					exit(CPack::ERR_PARAM_FORMAT);
				}

				pNode = new class CNode;
				pNode -> set_byte(static_cast<UCHAR>(iResultByte));
				pNode -> set_freq(uType.f);
				get_pdeq().push_back(pNode);
			}

			set_ptree(build_ptree_r(get_pdeq(), static_cast<int>(get_pdeq().size())));
			
			size_t iPos;
			size_t iPosMax = (get_sz_end() - 1u); // ignore last byte
			int iGet;
			int iByteLenLeft;
			PNODE pStart;
			pStart = get_ptree();
			bool bLastByte = true;
			int iCondition = 0; // by defaut

			for(iPos = 0; iPos < iPosMax; iPos += 1u) {
				goto_lastByte:;

				iByteLenLeft = 8;
				iGet = fgetc(get_buf_file());

				if(iGet == EOF) {
					exit(CPack::ERR_EOF);
				}

				do {
					--iByteLenLeft;

					if(iGet & (1 << iByteLenLeft)) {
						pStart = pStart -> get_right();
					} else {
						pStart = pStart -> get_left();
					}

					if(pStart == NULL) {
						exit(CPack::ERR_TREE_RESULT);
					}

					if(((pStart -> get_right()) == NULL) && ((pStart -> get_left()) == NULL)) {
						set_sz_begin(get_sz_begin() + 1u);
						fputc(static_cast<int>(pStart -> get_byte()), get_target());
						pStart = get_ptree();
					}
				} while(iByteLenLeft > iCondition);
			}

			if(bLastByte) {
				bLastByte = false;
				iCondition = static_cast<int>(get_null_bc());
				goto goto_lastByte;
			}

			fclose(get_target());
			pDeCompressed = NULL;

			clear_pdeq();
			clear_ldeq();
			set_cratio((static_cast<float>(get_sz_end()) / get_sz_begin()));
			reset_data();
		} else {
		}
	} else {
	}
}

void CPack::decompress (FILE * pFile, size_t iMaxLen, const char * pNewName) {
	if(pFile) {
		set_buf_file(pFile);
		const char * psResult = gen_str_equal(pFile, iMaxLen);
		decompress(psResult, iMaxLen, pNewName);
		delete [] psResult;
	} else {
	}
}

void CPack::c_fill_out (void) {
	size_t iTotalLen = (get_sz_begin());
	size_t const icByteLen = 8u;
	const char * pNextByte = get_src();
	size_t iByte = 0;
	size_t iCode = 0;
	size_t iCodeLen = 0;
	size_t iCodeLenLeft = 0;
	size_t iByteLenLeft = icByteLen;

	while(true) {
		if(iTotalLen == 0) {
			if(iByteLenLeft) {
				if(iByteLenLeft != icByteLen) {
					set_null_bc(static_cast<UCHAR>(iByteLenLeft));
					fputc(iByte, get_buf_file());
					set_sz_end(get_sz_end() + 1u);
				}
			}

			break;
		} else {
			iCode = get_mask(static_cast<const UCHAR>(*pNextByte));
			iCodeLen = get_mask_sz(static_cast<const UCHAR>(*pNextByte));
			iCodeLenLeft = iCodeLen;
		}

		goto_ignoreCondition:;

		if(iByteLenLeft == 0) {
			fputc(iByte, get_buf_file());
			set_sz_end(get_sz_end() + 1u);
			iByte = 0;
			iByteLenLeft = icByteLen;

			if(iCodeLenLeft == 0) {
				++pNextByte;
				iTotalLen -= 1u;

				continue;
			} else {
				goto goto_ignoreCondition;
			}
		} else if(iCodeLenLeft < iByteLenLeft) {
			iByte |= ((iCode >> (iCodeLen - iCodeLenLeft)) << (iByteLenLeft - iCodeLenLeft));
			iByteLenLeft -= iCodeLenLeft;
			++pNextByte;
			iTotalLen -= 1u;

			continue;
		} else if(iCodeLenLeft == iByteLenLeft) {
			iByte |= ((iCode >> (iCodeLen - iCodeLenLeft)) << (iByteLenLeft - iCodeLenLeft));
			iByteLenLeft = 0;
			iCodeLenLeft = 0;

			goto goto_ignoreCondition;
		} else { // iCodeLenLeft > iByteLenLeft
			iCodeLen -= iByteLenLeft;
			iByte |= (iCode >> iCodeLen);
			iCode &= gen_bitMaskByShift(1u, (iCodeLen - 1u));
			iCodeLenLeft = iCodeLen;
			iByteLenLeft = 0;

			goto goto_ignoreCondition;
		}
	}

	set_cratio((static_cast<float>(get_sz_end()) / get_sz_begin()));
	fputs(HAFFMAN_FIRST_INFO, get_target());
	fputc('\n', get_target());

	const size_t ciSizeMax = ((sizeof(size_t) * 8u) + def_strLen("?? ") + 1u);
	char sLine[ciSizeMax];

	_snprintf(sLine, ciSizeMax, "bc %d", get_sz_end()); // "bc" - byte count (compressed)
	fputs(sLine, get_target());
	fputc('\n', get_target());
	_snprintf(sLine, ciSizeMax, "bu %d", get_bdeq().size()); // "bu" - byte used (total)
	fputs(sLine, get_target());
	fputc('\n', get_target());
	_snprintf(sLine, ciSizeMax, "nb %d", static_cast<size_t>(get_null_bc())); // "nb" - null bits (end)
	fputs(sLine, get_target());
	fputc('\n', get_target());
	_snprintf(sLine, ciSizeMax, "hs %s", NULL); // "hs" - hashsum
	fputs(sLine, get_target());
	fputc('\n', get_target());

	uShiftTypization uType;

	for(PACK_CONTAINER::iterator x = get_bdeq().begin(); x != get_bdeq().end(); ++x) {
		uType.f = ((*x) -> get_freq());
		_snprintf(sLine, ciSizeMax, "%d %d", static_cast<size_t>((*x) -> get_byte()), uType.i);
		fputs(sLine, get_target());
		fputc('\n', get_target());
	}

	rewind(get_buf_file()); // fseek(get_buf_file(), 0, SEEK_SET);
	
	int iNewByte;

	do {
		iNewByte = fgetc(get_buf_file());

		if(iNewByte == EOF) {
			break;
		}

		fputc(static_cast<char>(iNewByte), get_target());
	} while(true);
}

void CPack::compress (const char * pSrc, size_t iMaxLen, const char * pNewName) {
	if(pSrc && iMaxLen && (get_src() == NULL)) {
		set_buf_file(fopen(HAFFMAN_BUF_FILENAME, "w+b"));

		if(get_buf_file()) {
			char * sFileName = NULL;

			if(pNewName) {
				size_t iLen = strlen(pNewName);

				if(iLen) {
					sFileName = new char [(iLen + def_strLen(HAFFMAN_EXTENSION) + 1u)];
					memcpy(sFileName, pNewName, iLen);
					memcpy((sFileName + iLen), HAFFMAN_EXTENSION, def_strLen(HAFFMAN_EXTENSION));
					sFileName[(iLen + def_strLen(HAFFMAN_EXTENSION))] = '\0';
				} else {
				}
			} else {
				sFileName = new char [(def_strLen(HAFFMAN_DEFAULT_OUT_NAME) + def_strLen(HAFFMAN_EXTENSION) + 1u)];
				memcpy(sFileName, HAFFMAN_DEFAULT_OUT_NAME, def_strLen(HAFFMAN_DEFAULT_OUT_NAME));
				memcpy((sFileName + def_strLen(HAFFMAN_DEFAULT_OUT_NAME)), HAFFMAN_EXTENSION, def_strLen(HAFFMAN_EXTENSION));
				sFileName[(def_strLen(HAFFMAN_DEFAULT_OUT_NAME) + def_strLen(HAFFMAN_EXTENSION))] = '\0';
			}

			FILE * pCompressed = fopen(sFileName, "wb");
			delete [] sFileName;
			sFileName = NULL;

			if(pCompressed) {
				set_src(pSrc);
				set_target(pCompressed);
				set_sz_begin(iMaxLen);
				set_btree(new class CNode);
				set_count(static_cast<const UCHAR>(pSrc[0]), 1u);
				get_btree() -> set_byte(static_cast<const UCHAR>(pSrc[0]));
				get_bdeq().push_back(get_btree()); // first 'in'

				size_t i;
			
				for(i = 1u; i < iMaxLen; i += 1u) {
					build_btree_r(get_btree(), static_cast<const UCHAR>(pSrc[i]));
				}

				float fPercent;
				UCHAR uByte;
			
				for(PACK_CONTAINER::iterator x = get_bdeq().begin(); x != get_bdeq().end(); ++x) {
					uByte = ((*x) -> get_byte());
					fPercent = (static_cast<float>(get_count(uByte)) / get_sz_begin());
					(*x) -> set_freq(fPercent);
					set_freq(uByte, fPercent);
					(*x) -> set_left(NULL);
					(*x) -> set_right(NULL);
				}

				get_pdeq() = get_bdeq();
				set_ptree(build_ptree_r(get_pdeq(), static_cast<int>(get_pdeq().size())));
				c_make_codes_r(get_ptree());
				c_expand_info ();
				c_fill_out();
				fclose(get_target());
				fclose(get_buf_file());

				if(remove(HAFFMAN_BUF_FILENAME) != 0) {
					exit(CPack::ERR_BUF);
				}
				
				clear_info();
				clear_bdeq();
				clear_pdeq();
				clear_ldeq();
				reset_data();

				pCompressed = NULL;
			} else { // no compressed file
			}
		} else { // no buf file
		}
	} else { // bad input
	}
}

void CPack::reset_data (void) {
	set_sz_begin (0);
	set_sz_end (0);
	set_btree (NULL);
	set_ptree (NULL);
	set_src (NULL);
	set_target(NULL);
	set_null_bc (0);
	set_buf_file(NULL);
	clear_info();
}

CPack::CPack (void) {
	reset_data();
	set_cratio(0.0f);
}

CPack::~CPack (void) {
}