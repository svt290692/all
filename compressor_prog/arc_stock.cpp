#include "arc_stock.h"
#include "arc_pack.h"

size_t gen_bitMaskByShift(size_t iVar, size_t iDegree) {
	if((iVar == 1u) || ((iVar % 2) == 0)) {
		while(iDegree) {
			iVar |= (iVar << 1);
			iDegree -= 1u;
		}
	} else {
		iVar = 0;
	}
	
	return iVar;
}

char * skip_strDataPart (const char * pStr) {
	char c;

	while ((c = *pStr) && (c != ' ') && (c != '\t')) {
		pStr++;
	}

	return const_cast<char *>(pStr);
}

const char * gen_str_equal(FILE * pFile, size_t & iLenBack) {
	size_t lSize;
	iLenBack = 0;

	fseek (pFile, 0, SEEK_END);
	lSize = ftell (pFile);
	rewind (pFile);

	char * pBuf = new char [(sizeof(char) * lSize)];
	size_t iResult = fread (pBuf, 1, lSize, pFile);

	if(iResult != lSize) {
		exit(CPack::ERR_STR_GEN);
	}

	iLenBack = iResult;

	return const_cast<const char *>(pBuf);
}