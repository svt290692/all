﻿#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include "arc_pack.h"
#include <time.h>
#include "arc_stock.h"

int main(int argc, char ** argv) {
	FILE * pFile = fopen("vm.txt", "rb");

	if(pFile) {
		clock_t iTime;
		iTime = clock();
		class CPack * pPack = new class CPack;

		pPack -> compress(pFile, 0, NULL);
		//pPack -> compress(" hello &Privet ..!# ", def_strLen(" hello &Privet ..!# "), NULL);

		std::cout << "\ncompress time = " << ((static_cast<double>(clock()) - iTime) * CLOCKS_PER_SEC);
		std::cout << "\ncompress ratio = " << (pPack -> get_cratio()) << std::endl;

		iTime = clock();
		FILE * pDecompress = fopen("out.haffman", "a+b");
		pPack -> decompress(pDecompress, 0, NULL);

		std::cout << "\ndecompress time = " << ((static_cast<double>(clock()) - iTime) * CLOCKS_PER_SEC);
		std::cout << "\ndecompress ratio = " << (pPack -> get_cratio()) << std::endl;

		// TEST
			/*FILE * pFileSrc = fopen("vm.txt", "rb");
			FILE * pFileResult = fopen("out", "rb");
			int i1;
			int i2;
			size_t iPosErr = 0u;
			int iErrors = 0;

			if(pFileSrc && pFileResult) {
				while(true) {
					i1 = fgetc(pFileSrc);
					i2 = fgetc(pFileResult);

					if(i1 != i2) {
						std::cout << "\nERROR (diff): 1 = " << i1 << "; 2 = " << i2 << "; shift[" << iPosErr << ']';
						++iErrors;
					}

					if(i1 == EOF) {
						std::cout << "\nDONE! -> errors = " << iErrors << " end pos = " << iPosErr;
			
						break;
					}

					iPosErr += 1u;
				}
			} else {

			}*/

			// END TEST

		delete pPack;
		pPack = NULL;
	} else {
	}

	int iPause;
	std::cin >> iPause;

	return 0;
}