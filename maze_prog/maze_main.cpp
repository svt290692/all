#include <iostream>
#include <list>
#include <vector>
#include <deque>

/*
	�������:
		1)������: ���-�� ����� ������ ���� == ������ ��������� (�.�. ��� ������ �����)
		2)�� ������� �� ��������, �������� � ������ ������� ���� ��������.
		3)������: ������ ������ ����� ���������� ����� (+ ��� ������ �������� � ����� �����, � ��� ����� ������� � ���������), �� ����������� ���������, ��� � ����� ����� 1 ������ ������!
		4)������: �����������: '@' - �����; '-' - ��������� �������; '+' - ����� ������; ' ' - ��������
		5)����� ������ ������� ������� - ������������ ����, ����� ����������� ������� - ����� ������ (����� ��������� � ��������� ���������)
*/

#ifndef __linux__ // win
	#include <time.h>	
	#include <windows.h>

	#define CLEAR_CMD "cls"

	enum eConsoleColor {
		CON_COLOR_BLACK = 0,
		CON_COLOR_BLUE,
		CON_COLOR_GREEN,
		CON_COLOR_CYAN,
		CON_COLOR_RED,
		CON_COLOR_MAGENTA,
		CON_COLOR_BROWN,
		CON_COLOR_LIGHT_GRAY,
		CON_COLOR_DARK_GRAY,
		CON_COLOR_LIGHT_BLUE,
		CON_COLOR_LIGHT_GREEN,
		CON_COLOR_LIGHT_CYAN,
		CON_COLOR_LIGHT_RED,
		CON_COLOR_LIGHT_MAGENTA,
		CON_COLOR_YELLOW,
		CON_COLOR_WHITE
	};
#else // linux
	#include <time.h>
	#include <stdlib.h>
	#include <unistd.h>
	#include <stdio.h>

	#define CLEAR_CMD "clear"
#endif

using namespace std;

#define MARKED_POINT '.'
#define BOY_BYTE 11
#define GIRL_BYTE 12

#ifndef __linux__
HANDLE g_stdOutPut = GetStdHandle(STD_OUTPUT_HANDLE);

void set_color(HANDLE pHandle, int iText, int iBackGround) {
    SetConsoleTextAttribute(pHandle, (WORD)((iBackGround << 4) | iText));
}
#endif

int gen_randNum (int _iMin, int _iMax) {
	return (_iMin + (rand() % (_iMax + 1 - _iMin)));
}

class CLocation;

class CLocation {
public:
	enum eNeighbourPos {
		NEIGHBOUR_FALSE = -1,
		NEIGHBOUR_LEFT = 0,
		NEIGHBOUR_RIGHT,
		NEIGHBOUR_UP,
		NEIGHBOUR_DOWN
	};

	~CLocation(void) {};
	explicit CLocation(size_t _x, size_t _y) {
		set_x_pos(_x);
		set_y_pos(_y);
		set_leftNeighbour(NULL);
		set_rightNeighbour(NULL);
		set_upNeighbour(NULL);
		set_downNeighbour(NULL);

		CLocation::find_neighbours(this);
		CLocation::add_location(this);
	};

	bool is_this(size_t _x, size_t _y) const {return ((get_x_pos() == _x) && (get_y_pos() == _y));};
	int is_neighbour(const class CLocation * _pLocation) const {return (is_neighbour((_pLocation -> get_x_pos()), (_pLocation -> get_y_pos())));};
	void add_neighbour(class CLocation * _pLocation) {m_listNeighbours.push_back(_pLocation);};
	size_t get_x_pos(void) const {return m_xPos;};
	size_t get_y_pos(void) const {return m_yPos;};
	void set_x_pos(size_t _i) {m_xPos = _i;};
	void set_y_pos(size_t _i) {m_yPos = _i;};
	class CLocation * get_leftNeighbour(void) const {return m_leftNeighbour;};
	class CLocation * get_rightNeighbour(void) const {return m_rightNeighbour;};
	class CLocation * get_upNeighbour(void) const {return m_upNeighbour;};
	class CLocation * get_downNeighbour(void) const {return m_downNeighbour;};
	void set_leftNeighbour(class CLocation * _p) {m_leftNeighbour = _p;};
	void set_rightNeighbour(class CLocation * _p) {m_rightNeighbour = _p;};
	void set_upNeighbour(class CLocation * _p) {m_upNeighbour = _p;};
	void set_downNeighbour(class CLocation * _p) {m_downNeighbour = _p;};
	void set_wave(size_t _i) {m_iWave = _i;};
	size_t get_wave(void) const {return m_iWave;};
	
	int is_neighbour(size_t _x, size_t _y) const {
		int iBack = CLocation::NEIGHBOUR_FALSE;
		
		if(_x == get_x_pos()) {
			if(get_y_pos() == (_y + 1)) {
				iBack = CLocation::NEIGHBOUR_UP;
			} else if(get_y_pos() == (_y - 1)) {
				iBack = CLocation::NEIGHBOUR_DOWN;
			}
		} else if(_y == get_y_pos()) {
			if(get_x_pos() == (_x + 1)) {
				iBack = CLocation::NEIGHBOUR_LEFT;
			} else if(get_x_pos() == (_x - 1)) {
				iBack = CLocation::NEIGHBOUR_RIGHT;
			}
		}

		return iBack;
	};

	static size_t get_exitZone(void) {return CLocation::s_iExitZone;};
	static size_t get_x_startPos(void) {return s_iStartPos_x;};
	static size_t get_y_startPos(void) {return s_iStartPos_y;};
	static size_t get_x_endPos(void) {return s_iEndPos_x;};
	static size_t get_y_endPos(void) {return s_iEndPos_y;};
	static void set_x_startPos(size_t _i) {s_iStartPos_x = _i;};
	static void set_y_startPos(size_t _i) {s_iStartPos_y = _i;};
	static void set_x_endPos(size_t _i) {s_iEndPos_x = _i;};
	static void set_y_endPos(size_t _i) {s_iEndPos_y = _i;};

	static void clear(void) {
		for(std::list<class CLocation *>::iterator x = CLocation::s_listAllLocations.begin(); x != CLocation::s_listAllLocations.end(); ++x) {
			delete (*x);
		}
	};

	static class CLocation * find_location(size_t _x, size_t _y) {
		class CLocation * pBack = NULL;

		for(std::list<class CLocation *>::iterator x = CLocation::s_listAllLocations.begin(); x != CLocation::s_listAllLocations.end(); ++x) {
			if((*x) -> is_this(_x, _y)) {
				pBack = (*x);

				break;
			}
		}

		return pBack;
	};

private:

	size_t m_iWave;
	class CLocation * m_leftNeighbour;
	class CLocation * m_rightNeighbour;
	class CLocation * m_upNeighbour;
	class CLocation * m_downNeighbour;
	size_t m_xPos;
	size_t m_yPos;
	std::list<class CLocation *> m_listNeighbours;

	static size_t s_iStartPos_x;
	static size_t s_iStartPos_y;
	static size_t s_iEndPos_x;
	static size_t s_iEndPos_y;
	static std::list<class CLocation *> s_listAllLocations;
	static const size_t s_iExitZone;

	static void add_location(class CLocation * _pLocation) {CLocation::s_listAllLocations.push_back(_pLocation);};

	static void find_neighbours(class CLocation * _pLocation) {
		for(std::list<class CLocation *>::iterator x = CLocation::s_listAllLocations.begin(); x != CLocation::s_listAllLocations.end(); ++x) {
			switch((*x) -> is_neighbour(_pLocation)) {
				case CLocation::NEIGHBOUR_FALSE: {
					goto goto_endSwitch;
				} case CLocation::NEIGHBOUR_LEFT: {
					(*x) -> set_leftNeighbour(_pLocation);
					_pLocation -> set_rightNeighbour(*x);

					break;
				} case CLocation::NEIGHBOUR_RIGHT: {
					(*x) -> set_rightNeighbour(_pLocation);
					_pLocation -> set_leftNeighbour(*x);

					break;
				} case CLocation::NEIGHBOUR_UP: {
					(*x) -> set_upNeighbour(_pLocation);
					_pLocation -> set_downNeighbour(*x);

					break;
				} case CLocation::NEIGHBOUR_DOWN: {
					(*x) -> set_downNeighbour(_pLocation);
					_pLocation -> set_upNeighbour(*x);

					break;
				} default: { // error
					system(CLEAR_CMD);
					std::cout << "\nerror! in function 'find_neighbours'";

					goto goto_findNeighboursEnd;
				}

				(*x) -> add_neighbour(_pLocation);
				_pLocation -> add_neighbour(*x);

				goto_endSwitch:;
			}
		}

		goto_findNeighboursEnd:;
	};

	explicit CLocation(const class CLocation &);
	explicit CLocation(const class CLocation *);
	explicit CLocation(void);
};

const size_t CLocation::s_iExitZone = 999999u;
std::list<class CLocation *> CLocation::s_listAllLocations;
size_t CLocation::s_iStartPos_x = 0;
size_t CLocation::s_iStartPos_y = 0;
size_t CLocation::s_iEndPos_x = 0;
size_t CLocation::s_iEndPos_y = 0;

class CMaze {
public:
	explicit CMaze(void) {
		FILE * pMazeCfg = fopen("maze.cfg", "r");

		if(pMazeCfg) {
			enum {MAZE_MAX_LINE_LEN = 64};
			char sLine[MAZE_MAX_LINE_LEN];
			size_t iMaxLineLen = 0;

			fgets(sLine, MAZE_MAX_LINE_LEN, pMazeCfg);
			iMaxLineLen = strlen(sLine);
			get_maze().push_back(new char [(iMaxLineLen + 1)]);
			memcpy(get_maze()[0], sLine, iMaxLineLen);
			get_maze()[0][iMaxLineLen] = '\0';
			size_t iPos = 1; // start from 1
			set_mazeLen(iMaxLineLen - 1u);

			while(fgets(sLine, MAZE_MAX_LINE_LEN, pMazeCfg)) {
				if(strlen(sLine) != iMaxLineLen) {
					system(CLEAR_CMD);
					std::cout << "\nmaze cfg has wrong line len :: line[" << iPos << ']' << std::endl;

					break;
				}

				get_maze().push_back(new char [(iMaxLineLen + 1)]);
				memcpy(get_maze()[iPos], sLine, iMaxLineLen);
				get_maze()[iPos][iMaxLineLen] = '\0';
				iPos += 1;
			}

			size_t y_pos;
			size_t x_pos;
			class CLocation * pLocation;
		
			for(y_pos = 1; y_pos < (get_maze().size() - 1u); y_pos += 1) {
				for(x_pos = 1; x_pos < (iMaxLineLen - 1u); x_pos += 1) {
					switch(get_maze()[y_pos][x_pos]) {
						case ' ': { // free way
							pLocation = new class CLocation(x_pos, y_pos);
							pLocation -> set_wave(0);

							break;
						} case '@': { // wall
							break;
						} case '-': { // start
							CLocation::set_x_startPos(x_pos);
							CLocation::set_y_startPos(y_pos);
							get_maze()[y_pos][x_pos] = 1;

							pLocation = new class CLocation(x_pos, y_pos);
							pLocation -> set_wave(1);
							set_pos(pLocation);

							break;
						} case '+': { // end
							CLocation::set_x_endPos(x_pos);
							CLocation::set_y_endPos(y_pos);
							get_maze()[y_pos][x_pos] = GIRL_BYTE;

							pLocation = new class CLocation(x_pos, y_pos);
							pLocation -> set_wave(CLocation::get_exitZone());

							break;
						} default: { // error
							system(CLEAR_CMD);
							std::cout << "\nmaze cfg has wrong symbols :: line[" << y_pos << "] pos[" << x_pos << ']' << std::endl;
							fclose(pMazeCfg);

							goto goto_wrongSymbols;
						}
					}
				}
			}

			fclose(pMazeCfg);
		} else {
			system(CLEAR_CMD);
			std::cout << "\ncan't open maze cfg" << std::endl;
		}

		drow();
		travel_r(get_pos());
		finish("\nAI error! maze locked :(", 1);

		goto_wrongSymbols:;
	}

	~CMaze(void) {
		CLocation::clear();
		size_t i;

		for(i = 0; i < get_maze().size(); i += 1) {
			delete [] get_maze()[i];
			get_maze()[i] = NULL;
		}
	};

private:

	size_t m_iMazeLen;
	class CLocation * m_pPos;
	std::vector<char *> m_vecMaze;

	static const size_t s_ciSleepTime;

	size_t get_mazeLen(void) const {return m_iMazeLen;};
	void set_mazeLen(size_t _i) {m_iMazeLen = _i;};
	std::vector<char *> & get_maze(void) {return m_vecMaze;};
	class CLocation * get_pos(void) const {return m_pPos;};
	void set_pos(class CLocation * _p) {m_pPos = _p;};

	static size_t get_sleepT(void) {return CMaze::s_ciSleepTime;};
	
	void finish(const char * pMsg, int iExitStat) {
		COORD iCoords = {0, static_cast<int>(get_maze().size())};
		SetConsoleCursorPosition(g_stdOutPut, iCoords);
		set_color(g_stdOutPut, CON_COLOR_WHITE, CON_COLOR_BLACK);
		std::cout << pMsg << std::endl;

	#ifndef __linux__ // win
		Sleep(CMaze::get_sleepT() * 50);
	#else // linux
		sleep(CMaze::get_sleepT() * 50);	
	#endif
		exit(iExitStat);
	};

	void move(class CLocation * pLocationOld, class CLocation * pLocationNew) {
		COORD iCoordsOld = {static_cast<int>(pLocationOld -> get_x_pos()), static_cast<int>(pLocationOld -> get_y_pos())};
		set_color(g_stdOutPut, CON_COLOR_LIGHT_GRAY, CON_COLOR_BLACK);
		get_maze()[(pLocationOld -> get_y_pos())][(pLocationOld -> get_x_pos())] = MARKED_POINT;
		putc(MARKED_POINT, stdout);
		SetConsoleCursorPosition(g_stdOutPut, iCoordsOld);
		set_color(g_stdOutPut, CON_COLOR_LIGHT_GREEN, CON_COLOR_BLACK);
		COORD iCoordsNew = {static_cast<int>(pLocationNew -> get_x_pos()), static_cast<int>(pLocationNew -> get_y_pos())};
		SetConsoleCursorPosition(g_stdOutPut, iCoordsNew);
		get_maze()[(pLocationNew -> get_y_pos())][(pLocationNew -> get_x_pos())] = BOY_BYTE;
		putc(BOY_BYTE, stdout);
		set_pos(pLocationNew);
		SetConsoleCursorPosition(g_stdOutPut, iCoordsNew);
	};

	void travel_r(class CLocation * _pLocation) {
		if((_pLocation -> get_wave()) == CLocation::get_exitZone()) {
			move(get_pos(), _pLocation);
			//drow();
			finish("\nAI escaped! :)", 0);
		} else {
			size_t iExit;
			int iPos;
			std::deque<class CLocation * (CLocation::*)(void) const> dequePtrs;

			dequePtrs.push_back(&CLocation::get_leftNeighbour);
			dequePtrs.push_back(&CLocation::get_rightNeighbour);
			dequePtrs.push_back(&CLocation::get_upNeighbour);
			dequePtrs.push_back(&CLocation::get_downNeighbour);

			class CLocation * (CLocation::*pCallBack)(void) const;
			iPos = gen_randNum(0, (static_cast<int>(dequePtrs.size()) - 1));
			pCallBack = dequePtrs[iPos];
			dequePtrs.erase(dequePtrs.begin() + iPos);
			
			if((_pLocation ->* pCallBack)()) {
				iExit = (((_pLocation ->* pCallBack)()) -> get_wave());

				if(iExit == 0) {
					(((_pLocation ->* pCallBack)()) -> set_wave((get_pos() -> get_wave()) + 1u));
				} else if(iExit != CLocation::get_exitZone()) {
					goto goto_skipFirst;
				}

				move(get_pos(), _pLocation);
				//drow();
#ifndef __linux__ // win
				Sleep(CMaze::get_sleepT());
#else // linux
				sleep(CMaze::get_sleepT());	
#endif
				travel_r(((_pLocation ->* pCallBack)()));
				goto_skipFirst:;
			}
				
			iPos = gen_randNum(0, (static_cast<int>(dequePtrs.size()) - 1));
			pCallBack = dequePtrs[iPos];
			dequePtrs.erase(dequePtrs.begin() + iPos);

			if((_pLocation ->* pCallBack)()) {
				iExit = (((_pLocation ->* pCallBack)()) -> get_wave());

				if(iExit == 0) {
					(((_pLocation ->* pCallBack)()) -> set_wave((get_pos() -> get_wave()) + 1u));
				} else if(iExit != CLocation::get_exitZone()) {
					goto goto_skipSecond;
				}

				move(get_pos(), _pLocation);
				//drow();
#ifndef __linux__ // win
				Sleep(CMaze::get_sleepT());
#else // linux
				sleep(CMaze::get_sleepT());	
#endif
				travel_r(((_pLocation ->* pCallBack)()));
				goto_skipSecond:;
			}
				
			iPos = gen_randNum(0, (static_cast<int>(dequePtrs.size()) - 1));
			pCallBack = dequePtrs[iPos];
			dequePtrs.erase(dequePtrs.begin() + iPos);

			if(((_pLocation ->* pCallBack)())) {
				iExit = (((_pLocation ->* pCallBack)()) -> get_wave());

				if(iExit == 0) {
					(((_pLocation ->* pCallBack)()) -> set_wave((get_pos() -> get_wave()) + 1u));
				} else if(iExit != CLocation::get_exitZone()) {
					goto goto_skipThird;
				}

				move(get_pos(), _pLocation);
				//drow();
#ifndef __linux__ // win
				Sleep(CMaze::get_sleepT());
#else // linux
				sleep(CMaze::get_sleepT());	
#endif
				travel_r(((_pLocation ->* pCallBack)()));
				goto_skipThird:;
			}

			iPos = 0;
			pCallBack = dequePtrs[iPos];
			dequePtrs.erase(dequePtrs.begin() + iPos);

			if(((_pLocation ->* pCallBack)())) {
				iExit = (((_pLocation ->* pCallBack)()) -> get_wave());

				if(iExit == 0) {
					(((_pLocation ->* pCallBack)()) -> set_wave((get_pos() -> get_wave()) + 1u));
				} else if(iExit != CLocation::get_exitZone()) {
					goto goto_skipFourth;
				}

				move(get_pos(), _pLocation);
				//drow();
#ifndef __linux__ // win
				Sleep(CMaze::get_sleepT());
#else // linux
				sleep(CMaze::get_sleepT());	
#endif
				travel_r(((_pLocation ->* pCallBack)()));
				goto_skipFourth:;
			}

			move(get_pos(), _pLocation);
			//drow();

#ifndef __linux__ // win
			Sleep(CMaze::get_sleepT());
#else // linux
			sleep(CMaze::get_sleepT());	
#endif
		}
	};

	void drow(void) {
		system(CLEAR_CMD);
#ifndef __linux__
		set_color(g_stdOutPut, CON_COLOR_BLUE, CON_COLOR_BLACK);
		size_t xPos;
		char byte;
		int iColorOld;
		int iColorNew;
#endif
		size_t yPos;

		for(yPos = 0; yPos < (get_maze().size()); yPos += 1) {
#ifndef __linux__
			for(xPos = 0; xPos < get_mazeLen(); xPos += 1) {
				byte = get_maze()[yPos][xPos];

				switch(byte) {
					case '@': {
						iColorNew = CON_COLOR_BROWN;
						
						break;
					} case BOY_BYTE: { // boy
						iColorNew = CON_COLOR_GREEN;

						break;
					} case GIRL_BYTE: { // girl
						iColorNew = CON_COLOR_RED;

						break;
					} case '.': {
						iColorNew = CON_COLOR_DARK_GRAY;

						break;
					} default: {
						iColorNew = CON_COLOR_BLACK;

						break;
					}
				}

				if(iColorOld != iColorNew) {
					iColorOld = iColorNew;
					set_color(g_stdOutPut, iColorNew, CON_COLOR_BLACK);
				}

				putc(byte, stdout);
			}

			std::cout << std::endl;
#else
			std::cout << get_maze()[yPos];
#endif
		}

		COORD iCoords = {static_cast<int>(CLocation::get_x_startPos()), static_cast<int>(CLocation::get_y_startPos())};
		SetConsoleCursorPosition(g_stdOutPut, iCoords);
	};
	 
	explicit CMaze(const class CMaze &);
	explicit CMaze(const class CMaze *);
};

#ifndef __linux__ // win
const size_t CMaze::s_ciSleepTime = static_cast<size_t>(0.1f * 1000);
#else // linux
const size_t CMaze::s_ciSleepTime = static_cast<size_t>(0.1f);
#endif

int main (int argc, char ** argv) {
	srand(static_cast<size_t>(time(NULL)));
	class CMaze * pMaze = new class CMaze;
	delete pMaze;
	pMaze = NULL;

	int iPause;
	std::cin >> iPause;

	return 0;
}