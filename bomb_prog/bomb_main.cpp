#include <iostream>

#ifndef __linux__ // win
	#include <windows.h>

	#define CLEAR_CMD "cls"

#else // linux
	#define CLEAR_CMD "clear"

	#include <time.h>
	#include <stdlib.h>
	#include <unistd.h>
#endif

class CBomb {
public:
	explicit CBomb(size_t);
	~CBomb(void);

private:
	float m_fBombFlyTime;
	size_t m_iHeight;
	float m_fDistToGo;
	float m_fBuff;
	float m_fDistTraveled;
	
	// get | set
	void set_distTraveled(float);
	float get_distTraveled(void) const;
	void set_buff(float);
	float get_buff(void) const;
	void set_height(size_t);
	size_t get_height(void) const;
	void set_distToGo(float);
	float get_distToGo(void) const;
	float get_flyT (void) const;
	void set_flyT (float);

	// api
	void think(void);
	float get_dist (void) const;

	// static
	static const float s_cfFreeVelocity;
	static const size_t s_ciSleepTime;

	static size_t get_sleepT(void);
	static float get_freeVel(void);

	// blocked
	explicit CBomb(const class CBomb &);
	explicit CBomb(const class CBomb *);
	explicit CBomb(void);
};

const float CBomb::s_cfFreeVelocity = 9.81f;

#ifndef __linux__ // win
const size_t CBomb::s_ciSleepTime = static_cast<size_t>(1.0f * 1000);
#else // linux
const size_t CBomb::s_ciSleepTime = static_cast<size_t>(1.0f);
#endif

void CBomb::set_distTraveled(float _f) {m_fDistTraveled = _f;}
float CBomb::get_distTraveled(void) const {return m_fDistTraveled;}
void CBomb::set_buff(float _f) {m_fBuff = _f;}
float CBomb::get_buff(void) const {return m_fBuff;}
void CBomb::set_distToGo(float _f) {m_fDistToGo = _f;}
float CBomb::get_distToGo(void) const {return m_fDistToGo;}
void CBomb::set_height(size_t _i) {m_iHeight = _i;}
size_t CBomb::get_height(void) const {return m_iHeight;}
float CBomb::get_flyT (void) const {return m_fBombFlyTime;}
void CBomb::set_flyT (float _f) {m_fBombFlyTime = _f;}

float CBomb::get_freeVel(void) {return CBomb::s_cfFreeVelocity;}
size_t CBomb::get_sleepT(void) {return CBomb::s_ciSleepTime;}

float CBomb::get_dist (void) const {return (get_flyT() * get_flyT() * CBomb::get_freeVel() * 0.5f);}

void CBomb::think(void) {
	while(true) {
		system(CLEAR_CMD);
		std::cout << "\n[BOMB MESSAGE]: BEGIN";
		std::cout << "\nflying time = [" << get_flyT() << ']' << " (in sec)";

		set_distToGo(get_height() - get_distTraveled());

		std::cout << "\ndistance left = [" << get_distToGo() << "] (in meters)";
		std::cout << "\ndistance left = [" << static_cast<size_t>((get_distToGo() / get_height()) * 100) << ']' << " (in '%')";
		std::cout << "\nflying speed = [" << (get_distTraveled() - get_buff()) << "] (in - m/s)";
		std::cout << "\n[BOMB MESSAGE]: END\n";

		set_buff(get_distTraveled());
#ifndef __linux__ // win
		Sleep(CBomb::get_sleepT());
#else // linux
		sleep(CBomb::get_sleepT());	
#endif
		set_flyT(get_flyT() + 1.0f);
		set_distTraveled(get_dist());

		if(get_distTraveled() >= static_cast<float>(get_height())) {
			break;
		}
	}
}

CBomb::CBomb(size_t iHeight) {
	set_flyT(0.0f);
	set_height(iHeight);
	set_distToGo(0.0f); // ?
	set_distTraveled(0.0f);
	set_buff(0.0f);

	think();
}

CBomb::~CBomb(void) {
	system(CLEAR_CMD);
	std::cout << "\n\nBOOM !!!" << std::endl;
}

// END

int main (int argc, char ** argv) {
	size_t iHeight = 0;

	std::cout << "specify bomb height = ";
	std::cin >> iHeight;
	std::cout << "\nbomb dropped!\n";
	
	class CBomb * pBomb = new class CBomb(iHeight);
	delete pBomb;

	int iPause;
	std::cin >> iPause;

	return 0;
}