#include <iostream>
#include <stdio.h>

typedef unsigned char UCHAR;

class CByteStat {
public:
	explicit CByteStat(const char *);
	~CByteStat(void);

private:
	enum {s_ciMaxBytesC = 256};

	size_t m_iBytesTotal;
	 
	float m_afFreq[CByteStat::s_ciMaxBytesC];
	char m_aByteCode[CByteStat::s_ciMaxBytesC];
	size_t m_aByteCount[CByteStat::s_ciMaxBytesC];

	size_t get_count(UCHAR) const;
	void set_count(UCHAR, size_t);

	float get_freq(UCHAR) const;
	void set_freq(UCHAR, float);
	char get_byte(UCHAR) const;
	void set_byte(UCHAR, char);

	size_t get_total(void) const;
	void set_total(size_t);

	explicit CByteStat(const class CByteStat &);
	explicit CByteStat(const class CByteStat *);
	explicit CByteStat(void);
};

size_t CByteStat::get_count(UCHAR _c) const {return m_aByteCount[_c];}
void CByteStat::set_count(UCHAR _c, size_t _i) {m_aByteCount[_c] = _i;}
size_t CByteStat::get_total(void) const {return m_iBytesTotal;}
void CByteStat::set_total(size_t _i) {m_iBytesTotal = _i;}

CByteStat::CByteStat(const char * psFileName) {
	FILE * pFile = fopen(psFileName, "r");

	if(pFile) {
		set_total(0);
		UCHAR uc;

		for(uc = 0; ; uc += 1) {
			set_byte(uc, '\0');
			set_freq(uc, 0.0f);
			set_count(uc, 0);

			if(uc == 255) {
				break;
			}
		}

		int iByte;

		while((iByte = fgetc(pFile)) != EOF) {
			set_count(static_cast<UCHAR>(iByte), get_count(static_cast<UCHAR>(iByte)) + 1u); // (iByte < 0) ?
			set_total(get_total() + 1u);
		}

		char * achSorted[CByteStat::s_ciMaxBytesC]; // memset(achSorted, '\0', (sizeof(char *) * CByteStat::s_ciMaxBytesC));

		for(uc = 0; ; uc += 1) {
			set_freq(uc, ((static_cast<float>(get_count(uc)) / get_total()) * 100));
			achSorted[uc] = new char;
			*(achSorted[uc]) = static_cast<char>(static_cast<int>(uc) - 128);

			if(uc == 255) {
				break;
			}
		}

		size_t iSwap;
		char * pBuf = NULL;

		while(true) {
			iSwap = 0;

			for(uc = 0; ; uc += 1) {
				if(get_freq(static_cast<UCHAR>(static_cast<int>(*(achSorted[uc])) + 128)) < get_freq(static_cast<UCHAR>(static_cast<int>(*(achSorted[(uc + 1u)])) + 128))) {
					pBuf = achSorted[uc];
					achSorted[uc] = achSorted[(uc + 1)];
					achSorted[(uc + 1)] = pBuf;
					iSwap += 1;
				}

				if(uc == 254) {
					break;
				}
			}

			if(iSwap == 0) {
				break;
			}
		}

		// output
		for(uc = 0; ; uc += 1) {
			std::cout << "\nbyte == \'" << static_cast<char>(static_cast<int>(*(achSorted[uc])) - 128) << "\' :: freq == " << get_freq(static_cast<UCHAR>(static_cast<int>(*(achSorted[uc])) + 128));
			delete achSorted[uc];
			achSorted[uc] = NULL;

			if(uc == 255) {
				break;
			}
		}

		fclose(pFile);
	} else {
		std::cout << "\nerror! can't open file" << std::endl;
	}
}

CByteStat::~CByteStat(void) {
	// void
}

float CByteStat::get_freq(UCHAR _i) const {return m_afFreq[_i];}
void CByteStat::set_freq(UCHAR _i, float _f) {m_afFreq[_i] = _f;}
char CByteStat::get_byte(UCHAR _i) const {return m_aByteCode[_i];}
void CByteStat::set_byte(UCHAR _i, char _c) {m_aByteCode[_i] = _c;}

int main (int argc, char ** argv) {
	if(argc > 1) {
		const char * sFileName = const_cast<const char *>(argv[1]);
		class CByteStat * pByteStat = new class CByteStat(sFileName);

		delete pByteStat;
	} else {
		std::cout << "\nerror! no file name (for analyze)" << std::endl;
	}
	
	int iPause;
	std::cin >> iPause;

	return 0;
}